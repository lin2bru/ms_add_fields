{include file="common/subheader.tpl" title=__("test_task") target="#test_task_settings"}
<div id="test_task_settings" class="in collapse">
    <div class="control-group">
        <label for="product_test_author" class="control-label">{__("test_task.test_author")}</label>
        <div class="controls">
            <input class="input-large" form="form" type="text" name="product_data[test_author]" id="product_test_author" size="55" value="{$product_data.test_author}" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="product_test_type">{__("test_task.test_type")}:</label>
        <div class="controls">
            <select class="span3" name="product_data[test_type]" id="product_test_type">
                <option value="1" {if $product_data.test_type == "1"}selected="selected"{/if}>{__("test_task.test_type_rom")}</option>
                <option value="2" {if $product_data.test_type == "2"}selected="selected"{/if}>{__("test_task.test_type_pov")}</option>
                <option value="3" {if $product_data.test_type == "3"}selected="selected"{/if}>{__("test_task.test_type_ras")}</option>
                <option value="4" {if $product_data.test_type == "4"}selected="selected"{/if}>{__("test_task.test_type_sti")}</option>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="product_test_comments">{__("test_task.test_comments")}:</label>
        <div class="controls">
            <textarea name="product_data[test_comments]" id="product_test_comments" cols="55" rows="2" class="input-large">{$product_data.test_comments}</textarea>
        </div>
    </div>
</div>